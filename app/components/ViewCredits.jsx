import React, {Component} from 'react';

export class ViewCredits extends Component{

	constructor(props) {
	  super(props);
	}

	render(){
		return (
			<nav className="view-credits">
				<div className="frame-logo candle">
					<a href="http://candlelabs.com.mx">

						<img className="logo" src={this.props.candle_logo} alt="CandleLabs_Logo" />

						<p className="phrase">Powered by <b> CandleLabs Technologies</b>
						</p>		

					</a>
				</div>

				<div className="separator"></div>

				<div className="frame-logo">
					<h1 className="title">CoffeeTime</h1>
					<figure>
						<img className="logo" src={this.props.coffee_time_logo} alt="CoffeetimeLogo"/>
					</figure>
				</div>
				
				<div className="separator"></div>

				<div className="status">
					<label>Número de resultados: <b> {this.props.users}</b></label>
					<br/>
					<label>Última víctima: 
							<b> {this.props.last_victim}</b></label>
				</div>

			</nav>
		)
	}
}