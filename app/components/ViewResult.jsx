import React, {Component} from 'react';

export class ViewResult extends Component{

	render(){

		let win_img = (this.props.win)? <img src="./assets/images/ok.png" /> : <div></div>;

		return(
			<div className="view-result">
				
				<div className="circle shadow">
					<div className={"fx-circle " + this.props.fx }>
						{win_img}
						<p>{this.props.msg}</p>
					</div>
				</div>

				<div>
					<button 	className="shadow"
								onClick={this.props.getUser}>
							Iniciar
					</button>
				</div>
			</div>
		)
	}
}