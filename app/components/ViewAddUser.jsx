import React, {Component} from 'react';
import {ViewUserBox} from './ViewUserBox'

export class ViewAddUser extends Component{

	constructor(props) {
		super(props);
		this.isEnter = this.isEnter.bind(this);
	}

	isEnter( event ){
		if(event.keyCode == 13)
			this.props.addName()
	}

	render(){
		return (
			<div className="view-add-user">
				
				<div className="component shadow">
					<h3 className="title">Nombre de participante</h3>

					<input 	type="text" 
							value={this.props.current_name}
							onChange={this.props.writeName}
							onKeyDown={this.isEnter}/>
					<div className="actions">
						<button onClick={this.props.restart}>
							Reiniciar
						</button>	
						<button onClick={this.props.addName}>
							Agregar
						</button>
					</div>
					
					<ViewUserBox 	list_users={this.props.list_users}
									deleteItem={this.props.deleteItem}/>	
				</div>

			</div>
		);
	}
}