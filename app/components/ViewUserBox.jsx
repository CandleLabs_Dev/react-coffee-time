import ReactCSSTransitionGroup from 'react-addons-css-transition-group' // ES6
import React,{Component} from 'react';

export class ViewUserBox extends Component{

	constructor(props) {
		super(props);
	}

	render(){

		const items = this.props.list_users.map( (element , index) => {
			return(
				<div key={element.name + index } className="item-box">
					<div className="item-num">
						<b>{index + 1}</b>
					</div>
					<div className="item-name">
						<p>{element.name}</p>
					</div>
					<div>
						<p 	id={index + '_item'}
							className="item-action-close"
							onClick={this.props.deleteItem}>
							x
						</p>
					</div>
				</div>
			);
		})

		return(
			<div className="box-users">
				<ReactCSSTransitionGroup	
						transitionName="item"
				          transitionEnterTimeout={500}
				          transitionLeaveTimeout={300}>
					{items}
				</ReactCSSTransitionGroup>
				
			</div>
		);
	}

}