//Importacion dependencias
import React,{Component} from 'react';
import {render} from 'react-dom';
import firebase from 'firebase';

//Importacion de componentes
import {ViewAddUser} from './components/ViewAdduser';
import {ViewCredits} from './components/ViewCredits';
import {ViewResult}  from './components/ViewResult';


//Configuracion de firebase
const config = {
	apiKey: "AIzaSyCr7dzOAxL4UCfDgXV9GUHmuShLEJFTY_M",
	authDomain: "react-coffee-time.firebaseapp.com",
	databaseURL: "https://react-coffee-time.firebaseio.com",
	storageBucket: "react-coffee-time.appspot.com",
	messagingSenderId: "109862195854"
};

//inicializacion
firebase.initializeApp( config );
var nameRef = firebase
				.database()
				.ref();

const lol = [
	'El café es una bebida especial!',
	'Frio o caliente, ¿Cómo lo prefiere?',
	'¿Conoce la leyenda de Kaldi y sus cabras?'
];

class App extends Component{

	constructor() {
	  super();
	
	  this.state = {
	  	coffee_time_logo : './assets/images/coffee_time_logo.png',
	  	candle_logo : './assets/images/candle_logo.png',
	  	msg : '¿Desea comenzar?',
	  	last_victim : 'none',
	  	current_name : '',
	  	list_users : [],
	  	winner : null,
	  	users : 0,
	  	win : false,
	  	fx : 'pulse'
	  };

	  this.writeName = this.writeName.bind(this);
	  this.addName = this.addName.bind(this);
	  this.getUser = this.getUser.bind(this);
	  this.restart = this.restart.bind(this);
	  this.deleteItem = this.deleteItem.bind(this);
	}

	componentWillMount() {
		nameRef.on( 'value' , snap => {
			let curr = snap.val();
			console.log('->' , curr )
			this.setState({ 
				users : curr.users,
	  			last_victim : curr.last_victim
			})

		})
	}

	writeName( event ){
		if(this.state.current_name.length >= 50){
			this.setState({
				msg : 'El nombre es muy largo, puede colocar un apodo, ¿quiza?',
				current_name : this.state.current_name.substring(0,this.state.current_name.length - 1)
			})
			return;
		}
		this.setState({
			current_name : event.target.value
		})
	}

	addName(){

		//Validacion de elemento vacio
		if(this.state.current_name == ''){
			this.setState({ msg : 'Escriba un nombre :)'})
			return;
		}

		//Validacion de elements
		if(this.state.list_users.length == 20 ){
			this.setState({ msg : 'Solo pueden haber 20 participantes' })
			return;
		}

		let curr = this.state.list_users;

		curr.push({ 
			name : this.state.current_name
		});

		this.setState({
			msg : lol[Math.floor((Math.random() * lol.length))],
			list_users : curr,
			current_name : '',
			win : false,
			winner : null
		})
	}

	getUser(){
		if(this.state.list_users.length < 2){
			this.setState({
				msg : 'Faltan participantes, agregue unos cuantos.'
			})
			return;
		}
		//Obtencion de numero aletorio
		let pos = Math.floor((Math.random() * this.state.list_users.length));
		let winner = this.state.list_users[ pos ];

		//Cambio de estado para compoenentes
		this.setState({
			win : true,
			winner : winner,
			msg : 'El elegido es: ' + winner.name + ' - ' + (pos + 1),
			users : this.state.users++,
			fx : 'win-fx'
		})

		//Insercion a la base de datos
		nameRef.set({
			last_victim : winner.name,
			users : this.state.users
		})
	}

	restart(){
		this.setState({
			list_users : [],
			fx : 'pulse'
		})
	}

	deleteItem( event ){
		let curr = event.target.id.split('_')[0];
		let arr = this.state.list_users;
		let new_list = arr.splice(curr, 1);
		
		new_list = arr;

		this.setState({
			list_users : new_list
		})
	}

	render(){
		return (
			<section className="base-app">

				<ViewCredits 	users={this.state.users}
								last_victim={this.state.last_victim}
								candle_logo={this.state.candle_logo}
								coffee_time_logo={this.state.coffee_time_logo}/>

				<ViewAddUser 	list_users={this.state.list_users}
								current_name={this.state.current_name}
								writeName={this.writeName}
								deleteItem={this.deleteItem}
								addName={this.addName}
								restart={this.restart}/>

				<ViewResult 	msg={this.state.msg}
								getUser={this.getUser}
								winner={this.state.winner}
								win={this.state.win}
								fx={this.state.fx}/>


			</section>
		)
	}
}

render(
	<App />,
	document.getElementById('app')
)